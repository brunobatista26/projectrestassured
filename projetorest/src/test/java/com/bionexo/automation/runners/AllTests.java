package com.bionexo.automation.runners;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.bionexo.automation.steps.Wgg;
import com.bionexo.automation.utils.HandleProperties;

import io.restassured.RestAssured;

@RunWith(Suite.class)
@SuiteClasses({  })
public class AllTests {

	@BeforeClass
	public static void setup() {
		// pegando a url de determinados ambientes
		RestAssured.baseURI = "http://" + HandleProperties.getValue("urlBase");
	}
}
