package com.bionexo.automation.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class HandleProperties {

	private static final String PROPERTIESQA = "./src/test/resources/qa.properties";
	private static final String PROPERTIESDEV = "./src/test/resources/dev.properties";
	private static final String PROPERTIESPROD = "./src/test/resources/prod.properties";
	private static final String PROPERTIESSANDBOX = "./src/test/resources/prod.properties";
	private static Properties properties;

	public static String getValue(String value) {

		String envirommentName = System.getProperty("enviromment");
		HandleProperties.properties = new Properties();
		try {
			switch (envirommentName) {
			case "qa":
				HandleProperties.properties.load(new FileInputStream(PROPERTIESQA));
				System.out.println("qa");
				break;
			case "prod":
				HandleProperties.properties.load(new FileInputStream(PROPERTIESPROD));
				System.out.println("prod");
				break;
			case "dev":
				HandleProperties.properties.load(new FileInputStream(PROPERTIESDEV));
				System.out.println("dev");
			case "sandbox":
				HandleProperties.properties.load(new FileInputStream(PROPERTIESSANDBOX));
				System.out.println("sandbox");
			default:
				HandleProperties.properties.load(new FileInputStream(PROPERTIESQA));
				System.out.println("qa2");
				break;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return HandleProperties.properties.getProperty(value);
	}

}
